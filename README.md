# gitpractice

git 과제를 위한 프로젝트

_NoChange폴더에 있는 소스는 수정하지 마세요._

_아래 설명한 브랜치에서 NoChange 폴더에 있는 소스를 사용하시면 됩니다._

_파일 수정을 위해 메모장을 사용하셔도 되지만, vscode라는 IDE가 있으니 설치하셔서 사용해 보는것도 권장드립니다._

----------

## 과제를 위한 Set up 하기 


*   상대방 git project를 Fork 합니다.


 > ![Fork](./image/Fork.PNG)



*   자신의 gitLab Home으로 들어가시면, Fork한 프로젝트가 생성된 것을 확인 하실 수 있습니다.


 > ![afterFork](./image/afterFork.PNG)



*   Fork한 프로젝트를 clone 한 후 아래 과제를 진행하시면 됩니다.


 > ![clone](./image/clone.PNG)


----------

## 상황 요약

_모든 브랜치는 다 생성되어 있습니다._ 


 > ![Issue](./image/Issue.PNG)



* [ ] 작업자 Me

 - branch "1-feature_Me" 에서 WorkSpace폴더에 StopSliding.html, HideShow.html 을 기능을 추가할 예정입니다..


* [ ] 작업자 Cammi

 - branch "2-feature_Cammi" 에서 WorkSpace폴더에 Chaining.html, HideShow.html 추가했습니다. ( 분기에 다 작업 해놨습니다. 수정 하실 필요가 없어요.)
 

----------

## 해야할 일 (과제)


1.   **나(me)는 기능 개발 분기("1-feature_Me")으로 이동합니다.**



2.   **나(me)는 해당 분기에서 PM으로 부터 소스리뷰 사항을 수정합니다.**


 > ![srcReview](./image/srcReview.PNG)



3.   **나(me)는 해당 분기에서 StopSliding.html을 추가한후 커밋합니다..**



4.   **나(me)는 해당 분기에서 Cammi가 작업한 분기를 병합 처리한 후 커밋 합니다..**


  `*병합 충돌 시 "1-feature_Me" (HideShow.html) 형상관리를 기준으로 해결합니다.*`



5.  **나(me)는 해당 분기 Master 분기에 병합 후 localPC Master를 원격분기(origin/Master)에 Push합니다.**


----------